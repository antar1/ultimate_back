from flask import Blueprint, Response
import psycopg2 as pg
import psycopg2.extras
import traceback2 as tb
import json
import dbConnection

import time

itemList_api = Blueprint('itemList_api', __name__)

@itemList_api.route("/itemList")
def handler():
    result = itemList()
    resp = Response(json.dumps(result))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

def itemList():

    db = dbConnection.Database()
    conn_res = db.pg_connection()
    if not conn_res['state']:
        print conn_res['message']
        return 'error'

    else:
        query = '''select name, quantity, rate, unit, id from item;'''
        conn = conn_res['conn']
        curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curs.execute(query)
        result = curs.fetchall()
        newlist = []
        print result
        for item in result:
            i = {}
            i['name'] = item['name']
            i['quantity'] = item['quantity']
            i['rate'] = str(item['rate'])
            i['unit'] = item['unit']
            i['id'] = item['id']
            newlist.append(i)

        print newlist

        curs.close()
        db.release_conn(conn)
        return newlist

