from flask import Blueprint, Response
import psycopg2 as pg
import psycopg2.extras
import traceback2 as tb
import json
import dbConnection

customerList_api = Blueprint('customerList_api', __name__)

@customerList_api.route("/customerList")
def customerList():

    db = dbConnection.Database()
    conn_res = db.pg_connection()
    if not conn_res['state']:
        print conn_res['message']
        return 'error'

    else:
        query = '''select name, address, balance from party where type = 1;'''
        conn = conn_res['conn']
        curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curs.execute(query)
        result = curs.fetchall()
        newlist = []
        print result
        for item in result:
            i = {}
            i['name'] = item['name']
            i['address'] = item['address']
            i['balance'] = item['balance']
            newlist.append(i)

        print newlist
    resp = Response(json.dumps(newlist))
    resp.headers['Access-Control-Allow-Origin'] = '*'

    curs.close()
    db.release_conn(conn)
    return resp