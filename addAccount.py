from flask import Blueprint, Response, request
import psycopg2 as pg
import psycopg2.extras
import traceback2 as tb
import json
import dbConnection
from collections import Counter
'''
type:
1: customer
2: suppplier
3: bank_account
4: cash
5: employee
6: purchase
7: sale
'''

addAccount_api = Blueprint('addAccount_api', __name__)

@addAccount_api.route("/addAccount", methods=['POST'])
def handler():
    result = addAccount()
    resp = Response(json.dumps(result))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

def addAccount():

    request_data = request.data
    print "requested data: ", request_data

    if type(request_data) is str:
        json_data = json.loads(request_data)
    else:
        json_data = request_data

    json_data_keys = json_data.keys()

    json_data_keys = [str(item) for item in json_data_keys]
    print "requested data keys: ", json_data_keys

    recomended_keys = ['name', 'address', 'contact', 'type']
    print "recomended_keys: ", recomended_keys

    if Counter(json_data_keys) != Counter(recomended_keys):
        print 'Invalid request data.'
        return 'Invalid request data.'
    else:
        a = str(json_data['type'])
        print a
        b = 0
        if a == 'customer':
            b = 1
        elif a == 'supplier':
            b = 2
        elif a == 'bank_account':
            b = 3
        elif a == 'cash':
            b = 4
        elif a == 'employee':
            b = 5
        else:
            print 'Invalid type.'
            return 'Invalid type.'
        print 'name: ', json_data['name']
        print 'address: ', json_data['address']
        print 'contact: ', json_data['contact']
        print 'type: ', json_data['type']

        query = '''insert into account (name, address, contact, a_type) values(%(name)s, %(address)s, %(contact)s, %(type)s)'''
        params = {
            'name': json_data['name'],
            'address': json_data['address'],
            'contact': json_data['contact'],
            'type': b
        }

        db = dbConnection.Database()
        conn_res = db.pg_connection()
        if not conn_res['state']:
            print conn_res['message']
            return 'error in database connection'

        else:
            
            conn = conn_res['conn']
            curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            curs.execute(query, params)
            result = curs.rowcount

            print "number of rows affected: ", result

            curs.close()
            conn.commit()
            db.release_conn(conn)

            return "Added."

