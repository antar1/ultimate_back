from flask import Blueprint, Response, request
import psycopg2 as pg
import psycopg2.extras
import traceback2 as tb
import json
import dbConnection
from collections import Counter

addItem_api = Blueprint('addItem_api', __name__)

@addItem_api.route("/addItem", methods=['POST'])
def handler():
    result = addItem()
    resp = Response(json.dumps(result))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

def addItem():

    pack_data = request.data
    print "data: ", pack_data

    if type(pack_data) is str:
        raw_data = json.loads(pack_data)
    else:
        raw_data = pack_data

    raw_data_keys = raw_data.keys()

    raw_data_keys = [str(item) for item in raw_data_keys]
    print raw_data_keys

    recomended_keys = ['name', 'unit', 'quantity', 'rate']
    print recomended_keys

    if Counter(raw_data_keys) != Counter(recomended_keys):
        print 'Invalid request data.'
        return 'Invalid request data.'
    else:
        print 'name: ', raw_data['name']
        print 'unit: ', raw_data['unit']
        print 'quantity: ', raw_data['quantity']
        print 'rate: ', raw_data['rate']

        query = '''insert into item (name, unit, quantity, rate) values(%(name)s, %(unit)s, %(quantity)s, %(rate)s)'''
        params = {
            'name': raw_data['name'],
            'unit': raw_data['unit'],
            'quantity': raw_data['quantity'],
            'rate': raw_data['rate']
        }

        db = dbConnection.Database()
        conn_res = db.pg_connection()
        if not conn_res['state']:
            print conn_res['message']
            return 'error in database connection'

        else:
            
            conn = conn_res['conn']
            curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            curs.execute(query, params)
            result = curs.rowcount

            print "number of rows affected: ", result

            curs.close()
            conn.commit()
            db.release_conn(conn)

            return "Added."

