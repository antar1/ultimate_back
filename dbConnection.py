import psycopg2 as pg
import psycopg2.extras
import psycopg2.pool
import traceback2 as tb
import json
import os


class Database:
    connection_pool = None
    inited = False

    def __init__(self):
        print Database.inited
        Database.inited = True
        
        if not Database.connection_pool:
            print "Instantiating!"
            print 'Reading credentials...'
            try:
                # if alias == 'test':
                #     cred = {'pghost': os.getenv('test_host'),
                #             'pguser': os.getenv('test_user'),
                #             'pgdatabase': os.getenv('test_database'),
                #             'pgpass': os.getenv('test_password')}
                # elif alias == 'prod':
                #     cred = {'pghost': os.getenv('prod_host'),
                #             'pguser': os.getenv('prod_user'),
                #             'pgdatabase': os.getenv('prod_database'),
                #             'pgpass': os.getenv('prod_password')}
                # else:
                #     return 'alias not set'
		filepath = os.path.dirname(os.path.abspath(__file__))
		print filepath
                dbconfigs = json.load(open(filepath+'/configs.json', 'r'))['db-configs']
                cred_name = dbconfigs['use-db']

                cred = {
                    'pghost': dbconfigs['dbs'][cred_name]['creds']['host'],
                    'pguser': dbconfigs['dbs'][cred_name]['creds']['username'],
                    'pgdatabase': dbconfigs['dbs'][cred_name]['creds']['database'],
                    'pgpass': dbconfigs['dbs'][cred_name]['creds']['password']
                }

                print 'Credentials got!!'
            except:
                print 'Error in reading from environment'
                tb.print_exc()

            try:
                print 'Establishing connection pool...'
                Database.connection_pool = pg.pool.SimpleConnectionPool(minconn=1, maxconn=5, dbname=cred['pgdatabase'], host=cred['pghost'], user=cred['pguser'], password=cred['pgpass'], port=5432)
                print 'Connection pool established.'

            except:
                print 'Error in making pool!!'
                tb.print_exc()

    def pg_connection(self):
        res = {}
        try:
            connection = Database.connection_pool.getconn()
            print 'Connection got from pool.'
            res['state'] = True
            res['conn'] = connection

        except:
            print 'Error in establishing pg connection'
            tb.print_exc()
            res['message'] = 'Error in establishing pg connection'
            res['state'] = False
        return res
        

    def release_conn(self, connection):
        try:
            Database.connection_pool.putconn(connection)

            # Database.connection_pool.closeall
            print("Put away a PostgreSQL connection")
        except (Exception, psycopg2.DatabaseError) as error :
            print ("Error while connecting to PostgreSQL", error)
