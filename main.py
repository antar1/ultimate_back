from flask import Flask
from getItemList import itemList_api
from getCustomerList import customerList_api
from addAccount import addAccount_api
from getAccountList import accountList_api
from addItem import addItem_api
from dbConnection import Database

app = Flask(__name__)
app.register_blueprint(itemList_api)
app.register_blueprint(customerList_api)
app.register_blueprint(addAccount_api)
app.register_blueprint(accountList_api)
app.register_blueprint(addItem_api)

@app.route('/')
def hello_world():
    print 'hello'
    return 'Hello World'

if __name__ == '__main__':
    app.debug = True
    Database()
    app.run()
