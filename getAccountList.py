from flask import Blueprint, Response, request
import psycopg2 as pg
import psycopg2.extras
import traceback2 as tb
import json
import dbConnection

accountList_api = Blueprint('accountList_api', __name__)

@accountList_api.route("/accountList")
def handler():
    result = accountList()
    resp = Response(json.dumps(result))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

def accountList():

    db = dbConnection.Database()
    conn_res = db.pg_connection()
    if not conn_res['state']:
        print conn_res['message']
        return 'error in database connection'

    else:
        needed = str(request.args.get('need'))
        print 'need: ', needed
        query = '''select name, address, contact, Case when a_type = 1 then 'Customer' when a_type = 2 then 'Supplier' when a_type = 3 then 'Bank Account' when a_type = 4 then 'Cash' when a_type = 5 then 'Employee' else 'Other' end as type, (debit-credit) as balance, a_type, id from account {data};'''

        if needed == 'customer':
            query = query.replace('{data}', 'where a_type = 1')
        elif needed == 'supplier':
            query = query.replace('{data}', 'where a_type = 2')
        elif needed == 'bank_account':
            query = query.replace('{data}', 'where a_type = 3')
        elif needed == 'employee':
            query = query.replace('{data}', 'where a_type = 4')
        elif needed == 'customer_supplier':
            query = query.replace('{data}', 'where a_type in (1,2)')
        else:
            query = query.replace('{data}', '')
        conn = conn_res['conn']
        curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curs.execute(query)
        result = curs.fetchall()
        newlist = []
        print result
        for item in result:
            i = {}
            i['name'] = item['name']
            i['address'] = item['address']
            i['contact'] = item['contact']
            i['type'] = item['type']
            i['balance'] = item['balance']
            i['id'] = item['id']
            if int(item['balance']) >= 0:
                i['drcr'] = 'DR'
            else:
                i['drcr'] = 'CR'
                i['balance'] = abs(int(i['balance']))
            
            newlist.append(i)
            # newlist.append(dict(zip(("name", "address", "contact", "type", "balance"), item)))

        print newlist

        curs.close()
        db.release_conn(conn)
        return newlist

