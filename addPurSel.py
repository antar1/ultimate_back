from flask import Blueprint, Response, request
import psycopg2 as pg
import psycopg2.extras
import traceback2 as tb
import json
import dbConnection
from collections import Counter

addPurSel_api = Blueprint('addPurSel_api', __name__)


@addPurSel_api.route("/addPurSel", methods=['POST'])
def handler():
    result = addPurSel()
    resp = Response(json.dumps(result))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


def addPurSel():

    print request.get_json()
    request_json = request.data
    if type(request_json) is str:
        request_json = json.loads(request_json)

    recomended_keys = ['account_id', 'date', 'remarks', 'items']
    request_json_keys = [str(item) for item in request_json.keys()]

    print 'recomended keys: ', recomended_keys
    print 'requested keys: ', request_json_keys

    if Counter(request_json_keys) != Counter(recomended_keys):
        print 'invalid keys'
        return 'Invalid keys'

    item_update_query = '''update item set quantity = quantity - %(pur_quantity)s where id = %(item_id)s;'''

    line_item_insert_query = '''insert into line_item (txn_id, account_id, debit, credit) values'''

    txn_item_insert_query = '''insert into txn_item_details (txn_id, item_id, quantity, rate) values'''

    txn_insert_query = '''insert into transaction (account_id, date, remarks, type) values (%(account)s, %(date)s, %(remarks)s, %(type)s) returning id;'''

    account_update_query = '''update account set credit = credit + %(credit)s , debit = debit + %(debit)s where id = %(account_id)s;'''

    txn_insert_params = {
        'account': str(request_json['account_id']),
        'date': str(request_json['date']),
        'remarks': str(request_json['remarks']),
        'type': 1
    }

    db = dbConnection.Database()
    conn_res = db.pg_connection()
    if not conn_res['state']:
        print conn_res['message']
        return 'error'
    conn = conn_res['conn']
    curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        curs.execute(txn_insert_query, txn_insert_params)
        txn_id = curs.fetchall()[0]['id']
        print 'transaction id: ',  txn_id
        total = 0

        for item in request_json['items']:
            print item
            item_update_params = {
                'pur_quantity': item['quantity'],
                'item_id': item['item_id']
            }

            curs.execute(item_update_query, item_update_params)
            print "updated: ", curs.rowcount

            txn_item_insert_params = {
                'txn': txn_id,
                'item': item['item_id'],
                'quantity': item['quantity'],
                'rate': item['rate']
            }

            txn_item_insert_query += curs.mogrify(
                '''(%(txn)s, %(item)s, %(quantity)s, %(rate)s), ''', txn_item_insert_params)

            total += float(item['quantity']) * float(item['rate'])

        txn_item_insert_query = txn_item_insert_query[0:-2] + ';'

        print "insert query: ", txn_item_insert_query

        curs.execute(txn_item_insert_query)
        print 'Txn item insert result: ', curs.rowcount

        line_item_insert_params = {
            'txn_id': txn_id,
            'account_id': request_json['account_id'],
            'debit': '0',
            'credit': total
        }

        line_item_insert_query += curs.mogrify(
            '''(%(txn_id)s, %(account_id)s, %(debit)s, %(credit)s), ''', line_item_insert_params)

        line_item_insert_params['debit'] = total
        line_item_insert_params['credit'] = '0'
        line_item_insert_params['account_id'] = '3'  # sale account

        line_item_insert_query += curs.mogrify(
            '''(%(txn_id)s, %(account_id)s, %(debit)s, %(credit)s);''', line_item_insert_params)

        print 'line_item_insert_query', line_item_insert_query

        curs.execute(line_item_insert_query)
        print 'Line item insert result: ', curs.rowcount

        account_update_params = {
            'debit': '0',
            'credit': total,
            'account_id': request_json['account_id']
        }

        curs.execute(account_update_query, account_update_params)
        print 'party account update result: ', curs.rowcount

        account_update_params['debit'] = total
        account_update_params['credit'] = '0'
        account_update_params['account_id'] = '3'

        curs.execute(account_update_query, account_update_params)
        print 'sale account update result: ', curs.rowcount
        curs.close()
        conn.commit()
    except:
        curs.close()
        print 'error'
        tb.print_exc()
    
    db.release_conn(conn)
    return 'done'
